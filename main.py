from faker import generate_fake_tag, generate_fake_value
import random
import json


def get(tag_names, sample_method, sample_period, simulate=False):
    values = {}
    for tag in tag_names:
        if simulate:
            values[tag] = generate_fake_value()
        else:
            raise Exception("Not yet supported.")

    return values


def get_tag_list(simulate=False):
    tags = []
    
    if simulate:
        # generate between 1-20 random tags
        # tags = [generate_fake_tag() for i in range(int(random.uniform(1, 20)))]
        # always return the same fake tags
        tags = ['001A36.PV', '031A39.PV', '001B46.PV', '021A36.PV', '001A896.PV', '001A22.PV', '001A67.PV', '002A36.PV', '001B36.PV']
    else:
        raise Exception("Not yet supported.")
    
    return tags


def put(tag_name, timestamp, value, simulate=False):
    raise Exception("Not yet supported.")


def akumen(method, params, simulate=False, **kwargs):
    """
    
    params:
        - Input: method [string]
        - Input: params [string]
        - Input: simulate [boolean]
    
        - Output: result [string]
      
    """
    print('Running Akumen model...')
    
    result = {}
    params = json.loads(params)
    
    if method == 'get':
        if not params.get('tag_names') or not params.get('sample_method') or not params.get('sample_period'):
            raise Exception('The `get` method requires three params: `tag_names`, `sample_method` and `sample_period`.')
        
        result = params
        result['values'] = get(params.get('tag_names'), params.get('sample_method'), params.get('sample_period'), simulate)
    elif method == 'get_tag_list':
        result['tags'] = get_tag_list(simulate)
    elif method == 'put':
        if not params.get('tag_name') or not params.get('timestamp') or not params.get('value'):
            raise Exception('The `put` method requires three params: `tag_name`, `timestamp` and `value`.')
        pass
    else:
        raise Exception('A valid method must be supplied: [get, get_tag_list, put]')
    
    return {
        'result': json.dumps(result)
    }